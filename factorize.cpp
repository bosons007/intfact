#include <stdio.h>
#include <iostream>
#include <string>
#include <string.h>
#include <gmp.h>

using namespace std;

const char* pi = "3141592653";
#include "e.hpp"

char* extract(const char* e, int i, int j) {
   std::string retStr = "";
   for (int k = i; k < j; ++k) {
        retStr += e[k];
   }
   return strdup(retStr.c_str());
}

char* getPiMask(const char* pi, char* e_mask) {
   int lpi = strlen(pi);
   std::string pistr = "";
   mpz_t trialNum;
   mpz_init(trialNum);
   std::string  pi_mask = "";
   mpz_t remainder;
   mpz_t divisor;
   for (int i = 0; i < lpi; ++i) {
       pistr += pi[i];
       mpz_set_str(trialNum, strdup(pistr.c_str()), 10);
       mpz_set_str(divisor, e_mask, 10);
       mpz_mod(remainder, trialNum, divisor);
       char* remStr = strdup(mpz_get_str(0, 10, remainder));
       pi_mask += remStr;    
   }
   return strdup(pi_mask.c_str());    
}

int main(int argc, char* argv[]) {
    char num[1024];
    strcpy(num, argv[1]);
    int l = strlen(num);
    // Read the number
    printf("\nNumber Read was: %s", num); 
    char* pi_mask = 0;
    for (int j = 0; j < 11; ++j) {
        for (int i = 0; i < 10; i += (j + 1)) {
            char* e_mask = extract(e, i, i+j);
            pi_mask = getPiMask(pi, e_mask);
        }
    }
    return 0;
}
